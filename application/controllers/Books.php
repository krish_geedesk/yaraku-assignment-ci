<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books extends CI_Controller 
{
	public function __construct() 
	{
		parent::__construct ();
		
		$this->load->dbutil();
	}

	public function index()
	{
		$this->load->view('app/header/header_view');
		$this->load->view('app/books/books_main_view');
		$this->load->view('app/footer/footer_view');
	}


	public function books_download_csv() 
	{
		$cur_date = Date('Y_m_d_H:i:s');

		$all_books = $this->books_model->get_all_books_csv_download();

		$filename = 'temp_files/books_'.$cur_date.'.csv';

		//Fetching the columns names for header
		$first_row = $all_books[0];

		//getting the headers from the arrays
		$headers = array_keys($first_row);

		// file creation
		$file = fopen($filename,"w");

		//Adding the headers to the csv file
		fputcsv($file,$headers);

		foreach ($all_books as $line)
		{
		 	fputcsv($file,$line);
		}

		fclose($file);

		$file_name = 'books_'.$cur_date.'.csv';

		// download
		header("Content-Description: File Transfer");
		header("Content-Disposition: attachment; filename=".$file_name);
		header("Content-Type: application/csv; "); 

		readfile($filename);

		// deleting file
		unlink($filename);
	}



	public function books_download_xml()
	{
		$cur_date = Date('Y_m_d_H:i:s');

		$query = $this->db->query("SELECT * FROM books");

		$file_name = 'books_'.$cur_date.'.xml';

		$config = array (
                  'root'    => 'books',
                  'element' => 'book',
                  'newline' => "\n",
                  'tab'    => "\t"
                );

		$xml = $this->dbutil->xml_from_result($query, $config);

		$this->load->helper('download');
		force_download($file_name, $xml);
	}


} //class ends
