<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');

class Books_api extends REST_Controller 
{

	public function __construct() 
	{
		parent::__construct();
	}


    public function add_new_book_post() 
    {
        $book_title = $this->post('book_title');
        $book_title = trim($book_title);
        $book_title = $this->security->xss_clean($book_title);

        $book_author = $this->post('book_author');
        $book_author = trim($book_author);
        $book_author = $this->security->xss_clean($book_author);

        $create_new_book = $this->books_model->add_new_book($book_title, $book_author);
        
        if ($create_new_book)
        {
            $this->response(array('status' => 'success', 'message' => 'Book created successfully'), 200);
        }
        else 
        {
            $this->response(array('status' => 'failed', 'message' => 'There was an error, please try again'), 200);
        }
    }



    public function all_books_get()
    {
        $search_query = $this->get('query');
        $search_query = trim($search_query);
        $search_query = $this->security->xss_clean($search_query);

        $all_books = $this->books_model->get_all_books($search_query);
        
        if (!empty($all_books)) 
        {
            $this->response($all_books, 200);
        }
        else 
        {
            $this->response(array('status' => 'failed', 'message' => 'No books available'), 200);
        }
    }



    public function delete_book_post() 
    {
        $book_id = $this->post('book');
        $book_id = trim($book_id);
        $book_id = $this->security->xss_clean($book_id);

        $delete_book = $this->books_model->delete_book($book_id);
        
        if ($delete_book)
        {
            $this->response(array('status' => 'success', 'message' => 'Book deleted successfully'), 200);
        }
        else 
        {
            $this->response(array('status' => 'failed', 'message' => 'There was an error, please try again'), 200);
        }
    }



    public function edit_book_post()
    {
        $book_id = $this->post('book');
        $book_id = trim($book_id);
        $book_id = $this->security->xss_clean($book_id);

        $book_author = $this->post('book_author');
        $book_author = trim($book_author);
        $book_author = $this->security->xss_clean($book_author);

        $edit_book = $this->books_model->edit_book_author($book_author, $book_id);
        
        if ($edit_book)
        {
            $this->response(array('status' => 'success', 'message' => 'Book author updated successfully'), 200);
        }
        else 
        {
            $this->response(array('status' => 'failed', 'message' => 'There was an error, please try again'), 200);
        }
    }

    

} //class ends