<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Books_model extends CI_Model 
{

	public function __construct() 
	{
		parent::__construct ();
		$this->load->database();
	}


    public function add_new_book($book_title, $book_author) 
    {
        $data = array(
            'book_title' => $book_title, 
            'book_author' => $book_author
        );

        return $this->db->insert('books', $data);
    }



    public function get_all_books($search_query) 
    {
        $this->db->select('id book_id, book_title, book_author');
        $this->db->from('books');

        if (!empty($search_query)) {
            $this->db->like('book_title', $search_query);
            $this->db->or_like('book_author', $search_query);
        }
        
        return $this->db->get()->result_array();
    }



    public function delete_book($book_id)
    {
        $this->db->where('id', $book_id);
        return $this->db->delete('books');
    }



    public function get_all_books_csv_download() 
    {
        $this->db->select('book_title Title, book_author Author');
        $this->db->from('books');
        return $this->db->get()->result_array();
    }



    public function edit_book_author($book_author, $book_id)
    {
        $data = array(
            'book_author' => $book_author
        );

        $this->db->where('id', $book_id);
        return $this->db->update('books', $data);
    }


} //class ends
