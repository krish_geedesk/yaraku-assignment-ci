<div id="books-app">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="form-group">
				<input type="text" class="form-control" v-model="bookTitle" placeholder="Book Title" :disabled="action==='edit'">
			</div>
			<br>
			<div class="form-group">
				<input type="text" class="form-control" v-model="bookAuthor" placeholder="Book Author">
			</div>
			<br>
			<span v-if="action === 'edit'">
				<button class="btn btn-sm btn-warning" @click="editBook">Edit</button>
			</span>
			<span v-else>
				<button class="btn btn-sm btn-primary" @click="addNewBook">Add</button>
			</span>
			<button class="btn btn-sm btn-info" @click="clearForm">Clear</button>
		</div>
		<div class="col-md-2"></div>
	</div>
	<br>
	<hr>
	<div class="row">
		<div class="col-md-5"></div>
		<div class="col-md-2"></div>
		<div class="col-md-2">
			<input type="text" placeholder="Search..." class="form-control" v-model="searchQuery" @keyup="searchBooks">
		</div>
		<div class="col-md-3">
			<a href="books/download/csv"><button class="btn btn-sm btn-primary">csv</button></a>
			<a href="books/download/xml"><button class="btn btn-sm btn-primary">xml</button></a>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div v-if="booksCount > 0">
				<table class="table">
					<thead>
						<tr>
							<th>Title</th>
				      		<th>Author</th>
				      		<th>Edit</th>
				      		<th>Delete</th>
				    	</tr>
				  	</thead>
				  	<tbody>
				  		<tr v-for="(item, index) in allBooks">
				  			<td>{{item.book_title}}</td>
				  			<td>{{item.book_author}}</td>
				  			<td>
				  				<button class="btn btn-sm btn-warning" @click="setEditBookVals(item.book_title, item.book_author, item.book_id)">Edit</button>
				  			</td>
				  			<td>
				  				<button class="btn btn-sm btn-danger" @click="deleteBook(item.book_id)">Delete</button>
				  			</td>
				    	</tr>
				  </tbody>
				</table>
			</div>
			<div v-else>
				No books available
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>
</div>