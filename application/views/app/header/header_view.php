<!DOCTYPE html>
<html>
<head>
	<base href="<?php echo base_url();?>">
	<title>Krishnan Yaraku Assignment</title>
	<link rel="stylesheet" href="static/css/lib/bootstrap.min.css">
	<script src="static/js/lib/jquery-3.5.1.slim.min.js"></script>
	<script src="static/js/lib/bootstrap.bundle.min.js"></script>

	<script src="static/js/lib/vue.min.js"></script>
	<script src="static/js/lib/axios.min.js"></script>
	<script src="static/js/lib/lodash.min.js"></script>
	<script src="static/js/lib/sweetalert2.all.min.js"></script>
</head>
<body>
	<div class="container">
		<h1 align="center">Books</h1>
		<br>