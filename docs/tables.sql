CREATE TABLE books (
id INT NOT NULL AUTO_INCREMENT,
book_title VARCHAR(500) NOT NULL unique,
book_author VARCHAR(500) NOT NULL, 
PRIMARY KEY (id)
)ENGINE = InnoDB;