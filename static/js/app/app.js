var app = new Vue({
	el: '#books-app', 
	data: {
        bookTitle: '', 
        bookAuthor: '', 
        allBooks: [], 
        booksCount: 0, 
        searchQuery: '', 
        action: ''
    },  
	mounted: function() {
        this.getAllBooks()
	}, 
	methods: {
        getAllBooks: function() {
            var self = this
            var url = 'api/books'
            axios.get(url)
            .then(function (response) {
                if (response.data.status != 'failed') {
                    self.allBooks = response.data
                    self.booksCount = self.allBooks.length
                }
                else {
                    self.allBooks = []
                    self.booksCount = 0
                }
            })
            .catch(function (error) {
                self.allBooks = []
                self.booksCount = 0
                console.log('error')
            })
        }, 
        addNewBook: function() {
            var self = this

            if (self.bookTitle === '' || self.bookTitle === null) {
                Swal.fire('Error!','Book title cannot be empty','error')
                return false
            }
            else if (self.bookAuthor === '' || self.bookAuthor === null) {
                Swal.fire('Error!','Book author cannot be empty','error')
                return false
            }

            var url = 'api/books/add'
            axios.post(url, {
                book_title : self.bookTitle, 
                book_author: self.bookAuthor
            })
            .then(function (response) {
                if (response.data.status === 'success') {
                    self.bookTitle  = ''
                    self.bookAuthor = ''
                    self.getAllBooks()
                    Swal.fire('Success!',response.data.message,'success')
                }
                else {
                    Swal.fire('Error!',response.data.message,'error')
                    console.log('failed')
                }
            })
            .catch(function (error) {
                Swal.fire('Error!','There was an error, please try again','error')
            })
        }, 
        deleteBook: function(bookId) {
            var self = this

            var url = 'api/books/delete'
            axios.post(url, {
                book: bookId
            })
            .then(function (response) {
                if (response.data.status === 'success') {
                    self.getAllBooks()
                    Swal.fire('Success!',response.data.message,'success')
                }
                else {
                    Swal.fire('Error!',response.data.message,'error')
                    console.log('failed')
                }
            })
            .catch(function (error) {
                Swal.fire('Error!','There was an error, please try again','error')
            })
        }, 
        searchBooks: function() {
            var self = this
            var url = 'api/books?query=' + self.searchQuery
            axios.get(url)
            .then(function (response) {
                if (response.data.status != 'failed') {
                    self.allBooks = response.data
                    self.booksCount = self.allBooks.length
                }
                else {
                    self.allBooks = []
                    self.booksCount = 0
                }
            })
            .catch(function (error) {
                self.allBooks = []
                self.booksCount = 0
                console.log('error')
            })
        }, 
        setEditBookVals: function(bookTitle, bookAuthor, bookId) {
            //Modifying the form for edit
            var self = this

            self.bookId         = bookId
            self.bookTitle      = bookTitle
            self.bookAuthor     = bookAuthor
            self.oldBookAuthor  = bookAuthor
            self.action         = 'edit'
        }, 
        editBook: function() {
            var self = this

            if (self.oldBookAuthor === self.bookAuthor) {
                Swal.fire('Error!','No changes to save','error')
                return false
            }
            else if (self.bookAuthor === '' || self.bookAuthor === null) {
                Swal.fire('Error!','Book author cannot be empty','error')
                return false
            }

            var url = 'api/books/edit'
            axios.post(url, {
                book        : self.bookId, 
                book_author : self.bookAuthor
            })
            .then(function (response) {
                if (response.data.status === 'success') {
                    self.bookId         = ''
                    self.bookTitle      = ''
                    self.bookAuthor     = ''
                    self.oldBookAuthor  = ''
                    self.action         = ''
                    self.getAllBooks()
                    Swal.fire('Success!',response.data.message,'success')
                }
                else {
                    Swal.fire('Error!',response.data.message,'error')
                    console.log('failed')
                }
            })
            .catch(function (error) {
                Swal.fire('Error!','There was an error, please try again','error')
            })
        },
        clearForm: function() {
            var self = this

            self.bookId         = ''
            self.bookTitle      = ''
            self.bookAuthor     = ''
            self.oldBookAuthor  = ''
            self.action         = ''
        }

    } //methods end
    
})